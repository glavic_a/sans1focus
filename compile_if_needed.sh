#!/bin/bash

if [ sans1exp.instr -nt sans1exp.out ] || [ ! -f sans1exp.out ]; then
	rm sans1exp.c sans1exp.out
	mcstas -o sans1exp.c sans1exp.instr $*
	mpicc -O2 -o sans1exp.out sans1exp.c -lm -DUSE_MPI \
	   -Wno-unused-result -Wno-format-truncation -Wno-format-overflow -Wno-stringop-overflow -Wimplicit-function-declaration
fi

if [ sans1focus.instr -nt sans1focus.out ] || [ ! -f sans1focus.out ]; then
	rm sans1focus.c sans1focus.out
	mcstas -o sans1focus.c sans1focus.instr $*
	mpicc -O2 -o sans1focus.out sans1focus.c -lm -DUSE_MPI \
	   -Wno-unused-result -Wno-format-truncation -Wno-format-overflow -Wno-stringop-overflow -Wimplicit-function-declaration
fi
