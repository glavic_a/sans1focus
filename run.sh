./compile_if_needed.sh

rm -rf results_exp
mkdir results_exp

mpirun -np 72 ./sans1exp.out -d results_exp/direct_beam -n 1e8 alpha_i=0
mpirun -np 72 ./sans1exp.out -d results_exp/single_0p6 -n 1e9 alpha_i=0.6 Itot=1.0e-1 gamma_x=9.0e-4 gamma_y=1.2e-4
mpirun -np 72 ./sans1exp.out -d results_exp/double_2p0 -n 1e9 alpha_i=2.0 Itot=1.0e-1 gamma_x=9.0e-4 gamma_y=1.2e-4
mpirun -np 72 ./sans1exp.out -d results_exp/double_3p0 -n 1e9 alpha_i=3.0 Itot=4.2e-2 gamma_x=2.5e-3 gamma_y=4.1e-4
mpirun -np 72 ./sans1exp.out -d results_exp/double_4p0 -n 1e9 alpha_i=4.0 Itot=4.4e-2 gamma_x=1.9e-2 gamma_y=9.3e-4
mpirun -np 72 ./sans1exp.out -d results_exp/double_5p0 -n 1e9 alpha_i=5.0 Itot=4.5e-2 gamma_x=1.8e-2 gamma_y=1.1e-3
mpirun -np 72 ./sans1exp.out -d results_exp/double_6p0 -n 1e9 alpha_i=6.0 Itot=4.2e-2 gamma_x=1.9e-2 gamma_y=1.3e-3


rm -rf results_focus
mkdir results_focus

mpirun -np 72 ./sans1focus.out -d results_focus/nograv_vs_08 -n 1e8 vs_size=0.008
mpirun -np 72 ./sans1focus.out -d results_focus/nograv_vs_20 -n 1e8 vs_size=0.020
mpirun -np 72 ./sans1focus.out -d results_focus/nograv_vs_40 -n 1e8 vs_size=0.040
mpirun -np 72 ./sans1focus.out -d results_focus/nograv_pinhole -n 1e8 vs_size=0.030 use_focusing=0

mpirun -np 72 ./sans1focus.out -d results_focus/sample_0200_vs_08   -n 1e8 vs_size=0.008 R_sample_sphere=200.0
mpirun -np 72 ./sans1focus.out -d results_focus/sample_0200_pinhole -n 1e8 vs_size=0.030 use_focusing=0 R_sample_sphere=200.0
mpirun -np 72 ./sans1focus.out -d results_focus/sample_0600_vs_08   -n 1e8 vs_size=0.008 R_sample_sphere=600.0
mpirun -np 72 ./sans1focus.out -d results_focus/sample_0600_pinhole -n 1e8 vs_size=0.030 use_focusing=0 R_sample_sphere=600.0
mpirun -np 72 ./sans1focus.out -d results_focus/sample_2000_vs_08   -n 1e8 vs_size=0.008 R_sample_sphere=2000.0
mpirun -np 72 ./sans1focus.out -d results_focus/sample_2000_pinhole -n 1e8 vs_size=0.030 use_focusing=0 R_sample_sphere=2000.0

python analysis/make_graphs.py
