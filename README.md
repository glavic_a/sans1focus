# SANS-1 Focus

Based on a SANS-1 experiment, the scattering of supermirror coated optics was
characterized. This repository contains a McStas model for estimating the
effect of such optics (PostMirrorScatter.comp), a model that reproduces
the experimental geometry (sans1expr.inst) and a estimation of the
effect on a high resolution optics addon (not yet implemented).
