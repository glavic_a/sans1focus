from matplotlib.pylab import *
from matplotlib.colors import LogNorm
import mcstas_reader as mr
import os

def LCDF(x1, x2, gamma, x0=0.):
    # calculate the integral over the Lorentzian from x1 to x2 (cummulative distribution function)
    return 1/pi*(arctan((x2-x0)/gamma)-arctan((x1-x0)/gamma))

def Lorentz(x, gamma, Itot, x0=0.):
    I = Itot*abs(x[1]-x[0]) # right scaling to make Itot the absolute scattering intensity
    return I*(1./pi*(gamma/2)/((x-x0)**2+(gamma/2)**2))

def plot_detector(name="direct_beam", Itot=6.3e-2, gamma_x=1.0e-3, gamma_y=2.1e-2):
    print(name)
    sim=mr.McSim(f'../results_exp/{name}')['detector']    
    figure(figsize=(8,6))
    colorbar(sim.plot())
    title(f'{name.replace("_"," ")} on detector')
    savefig(f'../results_exp/{name}.png', dpi=300)
    close()
    figure(figsize=(15,4))
    subplot(131)
    limits=sim.limits
    limits=arctan2(limits,1800.)*180./pi
    imshow(sim.data/sim.data.sum(), origin='lower', norm=LogNorm(1e-6,None), extent=limits)
    colorbar(label="Intensity [1]")
    title(f'{name.replace("_"," ")} on detector')
    xlabel('$\\phi_f$ [°]')
    ylabel('$2\\theta$ [°]')
    
    subplot(132)
    qx = 4*pi/10.*sin(linspace(limits[2], limits[3], 128)*pi/180./2)
    qmax = abs(qx).max()
    semilogy(qx, LCDF(-qmax, qmax, gamma_x)*Lorentz(qx, gamma_y, Itot), color='black')
    semilogy(qx, sim.data.sum(axis=0)/sim.data.sum())
    title("GISANS cut")
    xlabel("$q_{scatter}$ [Å$^{-1}$]")
    ylabel("Intensity [1/pixel]")
    ylim(1e-7,1)
    
    subplot(133)
    qy = 4*pi/10.*sin(linspace(limits[0], limits[1], 128)*pi/180./2)
    Iy = sim.data.sum(axis=1)/sim.data.sum()
    cen_y = (Iy*qy).sum()/Iy.sum()
    qy -= cen_y
    semilogy(qy, LCDF(-qmax, qmax, gamma_y)*Lorentz(qy, gamma_x, Itot), color='black')
    semilogy(qy, sim.data.sum(axis=1)/sim.data.sum())
    title("Off-specular cut")
    xlabel("$q_{scatter}$ [Å$^{-1}$]")
    ylabel("Intensity [1/pixel]")
    ylim(1e-7,1)


    tight_layout()
    savefig(f'../results_exp/log_{name}.png', dpi=300)
    close()

def plot_focus(name="nograv_vs_8"):
    print(f"focus {name}")
    sim=mr.McSim(f'../results_focus/{name}')['detector']
    figure(figsize=(8,6))
    colorbar(sim.plot())
    title(f"Direct Beam on Detector ({name})")
    xlabel("horizontal position [cm]")
    ylabel("vertical position [cm]")
    savefig(f'../results_focus/lin_{name}.png', dpi=300)
    close()

    figure(figsize=(8,6))
    imshow(sim.data, origin='lower', norm=LogNorm(1e-4,1e4), extent=sim.limits, cmap='gist_ncar')
    colorbar(label="Intensity [counts/s/pixel]")
    title(f"Direct Beam on Detector ({name})")
    xlabel("horizontal position [cm]")
    ylabel("vertical position [cm]")
    savefig(f'../results_focus/log_{name}.png', dpi=300)
    close()

def plot_sans(name="nograv_vs_8", direct_beam=None, step=0.8):
    # create radial integrated SANS pattern
    print(f"sans {name}")
    sim=mr.McSim(f'../results_focus/{name}')['detector']
    if direct_beam is not None:
        db=mr.McSim(f'../results_focus/{direct_beam}')['detector']
    else:
        db=sim
    x,y=meshgrid(sim.x, sim.y)
    cen_x=(db.x*db.xdata).sum()/db.xdata.sum()
    cen_y=(db.y*db.ydata).sum()/db.ydata.sum()
    r=sqrt((x-cen_x)**2+(y-cen_y)**2)
    Rmax=r.max()
    R = arange(0., Rmax+step, step)
    I = []
    I0 = db.data.sum()
    for Ri in R:
        fltr = (r>=Ri)&(r<(Ri+step))
        if fltr.sum()==0:
            I.append(nan)
        else:
            I.append(sim.data[fltr].mean()/I0)
    figure(figsize=(8,6))
    semilogy(4.*pi/10.0*sin(arctan2(R, 1800.0)/2.0), I)
    xlim(0., 0.025)
    ylim(1e-9, 1e-2)
    title(f"Sample Spheres ({name})")
    xlabel('q [Å${-1}$]')
    ylabel('I / I0')
    savefig(f'../results_focus/sans_{name}.png', dpi=300)

if __name__=='__main__':
    # change cwd to file location
    os.chdir(os.path.abspath(os.path.dirname(__file__)))

    plot_detector("direct_beam", Itot=1e-10)
    plot_detector("single_0p6", Itot=1.0e-1, gamma_x=9.0e-4, gamma_y=1.2e-4)
    plot_detector("double_2p0", Itot=1.0e-1, gamma_x=9.0e-4, gamma_y=1.2e-4)
    plot_detector("double_3p0", Itot=4.2e-2, gamma_x=2.5e-3, gamma_y=4.1e-4)
    plot_detector("double_4p0", Itot=4.4e-2, gamma_x=1.9e-2, gamma_y=9.3e-4)
    plot_detector("double_5p0", Itot=4.5e-2, gamma_x=1.8e-2, gamma_y=1.1e-3)
    plot_detector("double_6p0", Itot=4.2e-2, gamma_x=1.9e-2, gamma_y=1.3e-3)

    plot_focus("nograv_vs_08")
    plot_focus("nograv_vs_20")
    plot_focus("nograv_vs_40")
    plot_focus("nograv_pinhole")

    plot_sans("sample_0200_vs_08", "nograv_vs_08")
    plot_sans("sample_0200_pinhole", "nograv_pinhole")
    plot_sans("sample_0600_vs_08", "nograv_vs_08")
    plot_sans("sample_0600_pinhole", "nograv_pinhole")
    plot_sans("sample_2000_vs_08", "nograv_vs_08")
    plot_sans("sample_2000_pinhole", "nograv_pinhole")
